using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private InputField createID;
    [SerializeField] private InputField JoinID;
    [SerializeField] private GameObject lobbyPanel;
    [SerializeField] private Text waitInfo;
    [SerializeField] private int PlayerLimit = 2;

    public void CreateRoom() {
        PhotonNetwork.CreateRoom(createID.text);
    }
    public void JoinRoom()
    {
        PhotonNetwork.JoinRoom(JoinID.text);
    }
    public override void OnJoinedRoom()
    {

        if(PhotonNetwork.CurrentRoom.PlayerCount < PlayerLimit)
        UpdateUI();
        else if (PhotonNetwork.CurrentRoom.PlayerCount == PlayerLimit)
        {
            LoadScene();
        }
        else if (PhotonNetwork.CurrentRoom.PlayerCount > PlayerLimit)
        {
            // exit room or join other room
        }
    }

    private void LoadScene()
    {
        PhotonNetwork.LoadLevel("GameScene");
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        waitInfo.text = $"Waiting for other player {PhotonNetwork.CurrentRoom.PlayerCount}/{PlayerLimit}";
        if (PhotonNetwork.CurrentRoom.PlayerCount == PlayerLimit && PhotonNetwork.IsMasterClient )
        {
            LoadScene();
        }
    }

    private void UpdateUI()
    {
        lobbyPanel.SetActive(false);
        waitInfo.gameObject.SetActive(true);
        waitInfo.text = $"Waiting for other player {PhotonNetwork.CurrentRoom.PlayerCount}/{PlayerLimit}";
    }

}
