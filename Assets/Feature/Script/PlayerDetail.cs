using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDetail : MonoBehaviour
{

    [SerializeField] Text playerNameText;
    [SerializeField] Text playerScoreText;
    [SerializeField] Image timerBar;
    [SerializeField] GameObject timerObj;
    [SerializeField] float time = 5f;
    public int id = 0;
    public int score = 0;
    private string name = "";
    PhotonView view;
   public void SetDetail(int _id, string _name)
    {
        id = _id;       
        name = _name;
        playerNameText.text = _name;
        playerScoreText.text = "0";
    }
   
    public void StartMatch()
    {
       
        StartCoroutine(StartTimer());
    }
    [PunRPC]
    public void SetTurnInfo(int playerID)
    {
        if (GamePlayersHandler.Instance.IsMyTurn())
        {
            GamePlayersHandler.Instance.turnInfo.text = "Your Turn, Tap on the candles to Score";
            GamePlayersHandler.Instance.turnInfo.color = new Color(0, 1, 0, 1);
        }
        else
        {
            GamePlayersHandler.Instance.turnInfo.text = $"Player_{playerID} turn started";
            GamePlayersHandler.Instance.turnInfo.color = new Color(1, 0, 0, 1);
        }
    }
    IEnumerator StartTimer()
    {
        yield return new WaitForSecondsRealtime(2f); // 2 sec delay as per doc before next player turn
        GamePlayersHandler.Instance.currentActorNo = id;
        timerObj.SetActive(true);
        float timerCount = time;
        while(timerCount > 0)
        {
            yield return new WaitForSecondsRealtime(1f);
           
            timerCount--;
            float value =  (time - timerCount)/time;
            timerBar.fillAmount = value;
        }

        timerObj.SetActive(false);
        timerBar.fillAmount = 0;
        GamePlayersHandler.Instance.Nextturn();
    }
  
    [PunRPC]
    public void SetParent()
    {        
        transform.SetParent(GamePlayersHandler.Instance.spawnParent);
        view = this.gameObject.GetComponent<PhotonView>();
        GamePlayersHandler.Instance.AddPlayers(this);
        SetDetail(view.Controller.ActorNumber, $"Player_{view.Controller.ActorNumber}");
    }
    [PunRPC]
    public void SetScore(int candleIndex, int actorno)
    {
        GamePlayersHandler.Instance.candlesList[candleIndex].GetComponent<CandleHandler>().HideFlames();
        if (view.Controller.ActorNumber == actorno)
        {
            score++;
            playerScoreText.text = score.ToString();
        }
    }

}
