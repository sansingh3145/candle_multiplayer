using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class GamePlayersHandler : MonoBehaviourPunCallbacks
{

    [HideInInspector] public static GamePlayersHandler Instance;
    [HideInInspector] public PhotonView view;
    [HideInInspector] public List<PlayerDetail> playerList = new List<PlayerDetail>();
    [HideInInspector] public int playerTurn;
    [HideInInspector] public int currentActorNo;
    public Text turnInfo;
    public Transform spawnParent;
    public List<GameObject> candlesList = new List<GameObject>();
    [SerializeField] private Transform candleParent;

    private void Awake()
    {
        Instance =  this;
    }

    private void FixedUpdate()
    {
        candleParent.eulerAngles = new Vector3(
            candleParent.eulerAngles.x,
            candleParent.eulerAngles.y + 10,
            candleParent.eulerAngles.z
        );
    }

    private void Start()
    {
        GameObject playerObj = PhotonNetwork.Instantiate("playerDetail", Vector3.zero, Quaternion.identity);
        view = playerObj.GetComponent<PhotonView>();
        Invoke("CallAfter", 0.5f);
    }


    public bool IsMyTurn()
    {
        if(currentActorNo == view.Controller.ActorNumber)
        {
            return true;
        }
        return false;
    }

   
    public void AddPlayers(PlayerDetail _players)
    {
        playerList.Add(_players);
        if (playerList.Count == PhotonNetwork.CurrentRoom.PlayerCount)
        {
            view.RPC("SetTurnInfo", RpcTarget.All, playerList[playerTurn].id);
            playerTurn = 0;
            playerList[playerTurn].StartMatch();
        }
       
    }
   public void Nextturn()
    {
        currentActorNo = -1;
        playerTurn++;
        if (playerTurn < playerList.Count)
        {
            view.RPC("SetTurnInfo", RpcTarget.All, playerList[playerTurn].id);
            playerList[playerTurn].StartMatch();
        }
        else
            EndSequence();
    }
    private void EndSequence()
    {
        turnInfo.text = "All player turn completed";
        candleParent.gameObject.SetActive(false);
    }
    public void RemovePlayers(PlayerDetail _players)
    {
        playerList.Remove(_players);       
    }
   

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);

        PlayerDetail p = playerList.Find(x => x.id == otherPlayer.ActorNumber || x == null);

        RemovePlayers(p);
        
    }
    public void CallAfter()
    {
        view.RPC("SetParent", RpcTarget.AllBuffered);
    }

    public void UpdateScore(GameObject candle)
    {
        int candleIndex = candlesList.IndexOf(candle);
        view.RPC("SetScore", RpcTarget.AllBuffered, candleIndex, view.Controller.ActorNumber);
    }
   


}
