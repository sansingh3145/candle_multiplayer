using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandleHandler : MonoBehaviour
{
    [SerializeField] GameObject flames;
    bool flameOut = false;
    private void OnMouseDown()
    {
        if (GamePlayersHandler.Instance.IsMyTurn() && !flameOut)
        {
            flameOut = true;
            GamePlayersHandler.Instance.UpdateScore(this.gameObject);
        }

    }

    public void HideFlames()
    {
        flames.SetActive(false);
    }

}
